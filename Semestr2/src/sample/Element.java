package sample;

import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Polygon;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.Serializable;

/**
 * Created by ABC on 17.11.2014.
 */
public class Element implements Serializable {
    transient Element element = null;
    transient static ObjectOutputStream outputStream = null;
    String style1 = "-fx-fill: #9999a4;-fx-stroke: #48484d";
    String style2 = "-fx-fill: #bebec9;-fx-stroke: #9999a4";
    transient public Polygon polygon = null;
    private int size = 0;
    private int countPress = 0;
    private int own = 0;
    private int x = 0;
    private int y = 0;
    private final double R = 30;
    transient private Controller controller = null;
    transient Circle circle[] = null;
    transient Game game=null;

    Element(int size, int x, int y, double hexX, double hexY, Controller controller,Game game) {
        this.game=game;
        this.controller = controller;
        this.x = x;
        this.y = y;
        this.size = size;
        circle = new Circle[size];
        controller.mainField.getChildren().add(create(hexX, hexY));
        createPoint(hexX, hexY);
        element = this;
    }

    public Element() {
    }


    public int getSize() {
        return size;
    }

    public int getCountPress() {
        return countPress;
    }



    public boolean click(int q) {
        if(own==0&& game.getId()==q)
            game.setCountCurrent(game.getCountCurrent()+1);
        else if((own==0&& game.getId()!=q))
            game.setCountEnemy(game.getCountEnemy()+1);
        else
        {
            if(game.getId()==q) {
                game.setCountCurrent(game.getCountCurrent() + 1);
                game.setCountEnemy(game.getCountEnemy()-1);
            }
            else  {
                game.setCountCurrent(game.getCountCurrent() - 1);
                game.setCountEnemy(game.getCountEnemy()+1);
            }

        }
        if(game.isFirst()&&game.getCountCurrent()==0)
        {
            try {
                game.output.writeObject(null);
                game.output.flush();
                game.startNewGame();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else if(game.isFirst()&&game.getCountEnemy()==0)
        {
            try {
                game.output.writeObject(null);
                game.output.flush();
                game.startNewGame();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("click id= " + q + " current id =" + own);
            if (countPress == size - 1) {
                own = 0;
                style1 = "-fx-fill: #9999a4;-fx-stroke: #48484d";
                style2 = "-fx-fill: #bebec9;-fx-stroke: #9999a4";
                polygon.setStyle(style1);
                countPress = 0;
                for (int i = 0; i < size; i++) {
                    circle[i].setStyle("-fx-fill: #39383b");
                }
                return true;
            } else {
                if (q == 1) {
                    style1 = "-fx-fill: #980d0a;-fx-stroke: #48484d";
                    style2 = "-fx-fill: #980d0a;-fx-stroke: #9999a4";
                } else {
                    style1 = "-fx-fill: #177e13;-fx-stroke: #48484d";
                    style2 = "-fx-fill: #177e13;-fx-stroke: #9999a4";
                }
                circle[countPress].setStyle("-fx-fill: #efebf8");
                setOwn(q);
                countPress++;
                polygon.setStyle(style1);
                return false;
            }
        }
        return false;
    }

    public void copy(Element element) {
        style1 = element.style1;
        style2 = element.style2;
    }

    public int getOwn() {
        return own;
    }

    public Element setOwn(int own) {
        this.own = own;
        return this;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Polygon create(double x, final double y) {
        double r = Math.sqrt(3) / 2 * R;
        Double points[] = {x + R, y, x + R / 2, y + r, x - R / 2, y + r, x - R, y, x - R / 2, y - r, x + R / 2, y - r};
        polygon = new Polygon();
        polygon.setStyle(style1);
        polygon.setStrokeWidth(3);
        polygon.setOnMouseEntered(new javafx.event.EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                polygon.setStyle(style2);
            }
        });
        polygon.setOnMouseExited(new javafx.event.EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                polygon.setStyle(style1);
            }
        });
        polygon.setOnMouseClicked(new javafx.event.EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                try {
                    outputStream.writeObject(element);
                    outputStream.flush();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (Game.turn && (Game.id == own || own == 0)) {

                    if (Game.id == 1) {
                        style1 = "-fx-fill: #980d0a;-fx-stroke: #48484d";
                        style2 = "-fx-fill: #980d0a;-fx-stroke: #9999a4";
                        polygon.setStyle(style2);
                    } else {
                        style1 = "-fx-fill: #177e13;-fx-stroke: #48484d";
                        style2 = "-fx-fill: #177e13;-fx-stroke: #9999a4";
                        polygon.setStyle(style2);
                    }

                    game.step(element);
                    Game.turn = false;

                }
            }
        });
        polygon.getPoints().addAll(points);
        return polygon;
    }

    private void createPoint(double x, double y) {
        x -= R - 5;
        for (int i = 0; i < size; i++) {
            x += 8;
            circle[i] = new Circle(3);
            circle[i].setCenterX(x);
            circle[i].setCenterY(y);
            circle[i].setStyle("-fx-fill: #39383b");
            controller.mainField.getChildren().add(circle[i]);
        }
    }

}
