package sample;

import Server.*;
import javafx.stage.Stage;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by ABC on 17.11.2014.
 */
public class Client {
    public static int id=0;
    Element elements[][] = null;
    int size=0;
    Socket socket=null;

    Client(Stage stage)
    {
        try {
            socket= new Socket("localhost",4444);
            Game game= new Game(socket,stage);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
