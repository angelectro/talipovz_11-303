package sample;

import java.io.IOException;
import java.io.ObjectInputStream;

/**
 * Created by ABC on 19.11.2014.
 */
public class ClientInput implements Runnable {
    private boolean stop = false;
    private boolean end = false;
    Game game = null;
    ObjectInputStream inputStream = null;

    ClientInput(ObjectInputStream inputStream, Game game) {
        this.game = game;
        this.inputStream = inputStream;
        new Thread(this).start();
    }

    public void stop() {
        stop = true;
    }

    @Override
    public void run() {

        try {
            while (!stop) {
                System.out.println("���� ����������...");
                boolean checkClassElement = inputStream.readBoolean();
                Object data = inputStream.readObject();
                if (!checkClassElement && ((Integer) data == 1 || (Integer) data == 2)) {
                    Game.id = (Integer) data;
                    game.setTurn((Integer) data == 1 ? true : false);
                } else {
                    System.out.println(data);
                    game.setData(data);
                    game.setTurn(true);
                    game.read(checkClassElement, data);
                }

            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }


    public boolean isEnd() {
        return end;
    }

    public void setEnd(boolean end) {
        this.end = end;
    }
}
