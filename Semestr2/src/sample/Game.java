package sample;

import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * Created by ABC on 21.11.2014.
 */
public class Game implements Runnable {
    private boolean first=false;
    private int countCurrent = 0;
    private int countEnemy = 0;
    static int id;
    private Element elements[][] = null;
    private int size = 3;
    private Socket socket;
    static boolean turn = false;
    private Object data;

    ClientInput clientInput = null;
    ObjectOutputStream output = null;
    Scanner scanner = null;
    private Stage stage = null;
    private Controller controller = null;

    public Game(Socket socket, Stage stage) {
        this.stage = stage;
        scanner = new Scanner(System.in);
        this.socket = socket;
        try {
            clientInput = new ClientInput(new ObjectInputStream(socket.getInputStream()), this);
            output = new ObjectOutputStream(socket.getOutputStream());
            Element.outputStream = output;
            startNewGame();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void read(final boolean checkClassElement, final Object data) throws IOException {
        if (!checkClassElement && (Integer) data == -101) {
            System.out.println("��������� ����� �� ����.\n 1-�����\n 2-������ ����� ����");
            int change = scanner.nextInt();
            if (change == 1) {
                clientInput.stop();
            } else {
                output.writeInt(-100);
                output.flush();
                id = 0;
                startNewGame();
            }
        } else {
            Task task = new Task() {
                @Override
                protected Object call() throws Exception {
                    System.out.println("step");
                    step(data);
                    return null;
                }
            };
            Thread thread = new Thread(task);
            thread.setDaemon(true);
            Platform.runLater(thread);

        }


    }

    public void step(Object data) {
        Element element = (Element) data;
        if (element.game == this)
            element.setOwn(Game.id);
        System.out.println(element.getX() + " " + element.getY());
        final int id = elements[element.getX()][element.getY()].getOwn();
        if (elements[element.getX()][element.getY()].click(element.getOwn())) {
            LinkedList<Element> linkedList = new LinkedList<Element>();
            linkedList.push(elements[element.getX()][element.getY()]);

            while (linkedList.size() != 0) {
                final LinkedList<Element> temp = new LinkedList<Element>();
                final LinkedList<Element> finalLinkedList = linkedList;
                Task task = new Task() {
                    @Override
                    protected Object call() throws Exception {
                        while (finalLinkedList.size() != 0) {
                            Element element1 = finalLinkedList.poll();
                            blowUp(element1, temp, id);
                        }
                        return null;
                    }
                };
                Thread thread = new Thread(task);
                thread.start();
                try {
                    thread.join();
                    linkedList = temp;
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
        ;
    }

    public void startNewGame() {
        Platform.runLater(new Thread(this));
    }

    private void createField() {
        int bias = 4;
        double R = 30;
        double r = Math.sqrt(3) / 2 * R;
        int x = (int) (R + R / 2) * size;
        int y = 20;
        elements = new Element[size][size];
        for (int i = 0; i < size; i++) {
            int xT = x;
            int yT = y;
            for (int j = 0; j < size; j++) {
                if (i == 0) {
                    if (j == 0)
                        elements[i][j] = new Element(3, i, j, xT, yT, controller, this);
                    else if (j == size - 1)
                        elements[i][j] = new Element(2, i, j, xT, yT, controller, this);
                    else
                        elements[i][j] = new Element(4, i, j, xT, yT, controller, this);
                } else if (i == size - 1) {
                    if (j == 0)
                        elements[i][j] = new Element(2, i, j, xT, yT, controller, this);
                    else if (j == size - 1)
                        elements[i][j] = new Element(3, i, j, xT, yT, controller, this);
                    else
                        elements[i][j] = new Element(4, i, j, xT, yT, controller, this);
                } else {
                    if (j == 0 || j == size - 1)
                        elements[i][j] = new Element(4, i, j, xT, yT, controller, this);
                    else
                        elements[i][j] = new Element(5, i, j, xT, yT, controller, this);
                }
                xT += R + R / 2 + bias;
                yT += r + bias;
            }
            x -= R + R / 2 + bias;
            y += r + bias;
        }
    }

    public boolean isTurn() {
        return turn;
    }

    public void setTurn(boolean turn) {
        this.turn = turn;
    }

    private void blowUp(Element element, LinkedList<Element> temp, int id) {
        int x = element.getX();
        int y = element.getY();
        int xx[] = {-1, 0, -1, 1, 0, 1};
        int yy[] = {-1, -1, 0, 0, 1, 1};
        for (int i = 0; i < xx.length; i++) {
            if (x + xx[i] >= 0 && x + xx[i] < size && y + yy[i] >= 0 && y + yy[i] < size) {
                if (elements[x + xx[i]][y + yy[i]].click(id)) {
                    temp.push(elements[x + xx[i]][y + yy[i]]);
                }
            }
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCountEnemy() {
        return countEnemy;
    }

    public void setCountEnemy(int countEnemy) {
        this.countEnemy = countEnemy;
    }

    public int getCountCurrent() {
        return countCurrent;
    }

    public void setCountCurrent(int countCurrent) {
        this.countCurrent = countCurrent;
    }


    public void setData(Object data) {
        this.data = data;
    }

    @Override
    public void run() {
        System.out.println("�������� ����: ");
        int type = scanner.nextInt();
        try {
            output.writeInt(type);
            output.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("�������� ����������");
        while (id == 0) {
            System.out.print("");
        }
        size = 3 + type;
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("sample.fxml"));
        try {
            Parent parent = fxmlLoader.load();
            controller = fxmlLoader.getController();
            createField();
            stage.setScene(new Scene(parent, 600, 400));
            stage.show();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public boolean isFirst() {
        return first;
    }
}
