package Server;

import java.io.*;
import java.net.Socket;

/**
 * Created by ABC on 17.11.2014.
 */
public class Player {
    private Player enemy = null;
    private Socket socket = null;
    private String name = null;
    private ObjectOutputStream output = null;
    private ObjectInputStream input = null;
    private Game game;
    private int id;
    private int type;

    Player(Socket socket) {
        this.socket = socket;
        try {
            output = new ObjectOutputStream(socket.getOutputStream());
            this.input = new ObjectInputStream(socket.getInputStream());
        } catch (IOException e) {
            System.err.print("Error");
        }

    }

    public void start(Player e, int i, Game g) {
        this.game=g;
        this.enemy=e;
        this.id=i;
    }

    public String getName() {
        return name;
    }

    public ObjectOutputStream getOutput() {
        return output;
    }

    public ObjectInputStream getInput() {
        return input;
    }


    public void setId(int id) {
        this.id = id;
    }

    public void setGame(Game game) {
        this.game = game;
    }


    public int getType() {
        return type;
    }

    public Player getEnemy() {
        return enemy;
    }

    public Socket getSocket() {
        return socket;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }
}
