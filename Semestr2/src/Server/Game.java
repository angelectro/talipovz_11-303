package Server;

import java.io.IOException;

/**
 * Created by ABC on 17.11.2014.
 */
public class Game implements Runnable {
    Player player = null;
    Player playerWait = null;
    private boolean checkEnd = false;

    public Game(Player player1) {
        this.player = player1;
    }

    public void startGame(Player player2) throws IOException {
        this.playerWait = player2;
        System.out.println("Players " + player.getName() + " and " + playerWait.getName() + " begin the game");
        new Thread(this).start();
    }

    public void exchange() {
        Player temp = player;
        player = playerWait;
        playerWait = temp;
    }

    private boolean step() {
        Object coord = read(player);
        if (8 == -100) {// -100 ������� ��� ��������
            restart(player);
            restart(playerWait);
        }
        send(playerWait,true, coord);
        return false;
    }

    private void restart(Player player) {
        try {
            if (player.getInput().readInt() == -100) {
                System.out.println(100);
                Starter.push(player);
            }
        } catch (IOException e) {
            player = null;
        }
    }

    @Override
    public void run() {
        send(player,false, 1);
        send(playerWait,false, 2);
        player.start(playerWait, 1, this);
        playerWait.start(player, 2, this);

        while (!checkEnd) {
            step();
            exchange();
        }

    }

    private Object read(Player player) {
        Object o=null;
        try {
            o = player.getInput().readObject();
            System.out.println(o);
            if(o==null)
            {
                restart(player);
                restart(playerWait);
            }
        } catch (IOException e) {
            send(player.getEnemy(),false, -101);
            checkEnd=true;
            restart(player.getEnemy());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return o;
    }

    private void send(Player player, boolean check, Object coord) {
        try {

                player.getOutput().writeBoolean(check);
                player.getOutput().flush();
                player.getOutput().writeObject(coord);
                player.getOutput().flush();

        } catch (IOException e) {
            if (player.getSocket().isBound()) {
                checkEnd=true;
                send(player.getEnemy(), false, -101);
                restart(player.getEnemy());
            }
            else {
                System.err.println(player.getName() + " out of game");
            }
        }
    }
}
