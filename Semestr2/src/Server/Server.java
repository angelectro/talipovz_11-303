package Server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Created by ABC on 17.11.2014.
 */
public class Server implements Runnable{

    Server()
    {
        new Thread(this).start();
    }


    @Override
    public void run() {
        try {
            Starter starter= new Starter();
            ServerSocket  serverSocket= new ServerSocket(4444);
            while (true)
            {
                Socket socket=serverSocket.accept();
                System.out.println("accept");
                starter.push(new Player(socket));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
