package Server;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

/**
 * Created by ABC on 17.11.2014.
 */
public class Starter {
    private static int id=0;
    static volatile Game typeGame[]=null;
    static ArrayList<Game> games=null;
    Starter()
    {
        typeGame=new Game[3];
        games= new ArrayList<Game>();

    }

    public static void push(final Player player)
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    int type=player.getInput().readInt();
                    player.setId(id++);
                    System.out.println("connected: id " + player.getId() + " "+ "type: " +type);
                    if(typeGame[type]==null)
                    {
                        typeGame[type]= new Game(player);
                    }
                    else{
                        typeGame[type].startGame(player);
                        games.add(typeGame[type]);
                        typeGame[type]=null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }).start();

    }
}
