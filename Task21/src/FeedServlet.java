import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ABC on 01.10.14.
 */
public class FeedServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String username = Helpers.current_user(request);
        if (username != null) {
            response.setContentType("text/html");
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.println("Welcome  " + username+"<br/>" +
                    "<a href='/logout'>logout</a> ");
            writer.close();
        } else
            response.sendRedirect("/login");

    }
}
