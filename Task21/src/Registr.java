import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;

/**
 * Created by ABC on 08.10.14.
 */
public class Registr extends HttpServlet {
    private Connection connection;

    @Override
    public void init() throws ServletException {
        super.init();

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/Servak", "postgres", "amina"
            );
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("pass");
        String check_password = request.getParameter("check_pass");
        if (password.equals(check_password)) {
            try {
                PreparedStatement statement = connection.prepareStatement("select count(*) from users where username=?");
                statement.setString(1, username);
                ResultSet resultSet = statement.executeQuery();
                if (resultSet.next() && Integer.parseInt(resultSet.getString(1)) == 0) {
                    statement = connection.prepareStatement("insert into users (username,password) values(?,?)");
                    statement.setString(1, username);
                    statement.setString(2, password);
                    statement.executeQuery();
                } else {
                    response.sendRedirect("/registr?error=1");
                }
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }


        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = new PrintWriter(response.getOutputStream());
        String check = request.getParameter("error");
        writer.println("<html>\n" +
                "  <head>\n" +
                "    <title>Регистрация</title>\n" +
                "  </head>\n" +
                "  <body>\n" + (check != null ?
                "<p>Ползователь уже существует!</p>" : "") +
                "<form action='/registr' method='post'>" +
                "<input type='text' name='username'><br/>" +
                "<input type='password' name='pass'><br/>" +
                "<input type='password' name='check_pass'><br/>" +
                "<input type='submit' value='regist'></form>" +
                "\n" +
                "  </body>\n" +
                "</html>");
        writer.close();
    }
}
