import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.HashMap;

/**
 * Created by ABC on 01.10.14.
 */
public class LoginServlet extends HttpServlet {
    private Connection connection;

    @Override
    public void init() throws ServletException {
        super.init();

        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://127.0.0.1:5432/Servak", "postgres", "amina"
            );
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        ServletContext context = request.getServletContext();
        try {
            PreparedStatement statement=connection.prepareStatement("select count(*) from users where username=? and password=?");
            statement.setString(1,username);
            statement.setString(2,password);
            ResultSet resultSet=statement.executeQuery();
            if (resultSet.next()&& Integer.parseInt(resultSet.getString(1))!=0) {
                Cookie cookie = new Cookie("user", username);
                cookie.setMaxAge(3600);
                cookie.setPath("/feed");
                response.addCookie(cookie);
                HttpSession hs = request.getSession();
                hs.setAttribute("current_user", username);

                System.out.println("привет");
                response.sendRedirect("/feed");
            } else {
                response.sendRedirect("/login");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Cookie userCookie = Helpers.hetUserCookie(request);
        if (userCookie != null) {
            HttpSession hs = request.getSession();
            hs.setAttribute("current_user", userCookie.getValue());
        }
        if (Helpers.current_user(request) != null) {
            response.sendRedirect("/feed");
        } else {
            response.setContentType("text/html");
            PrintWriter writer = new PrintWriter(response.getOutputStream());
            writer.println("<form action='/login' method='post'>" +
                    "<input type='text' name='username'><br/>" +
                    "<input type='password' name='password'><br/>" +
                    "<input type='submit' value='login'>" +

                    "</form>" +
                    "<form  action='/registr'>" +
                    "<input type='submit' value='registr'>" +
                    "</form>");
            writer.close();
        }
    }
}
