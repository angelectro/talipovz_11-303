import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Paths;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ABC on 24.11.2014.
 */
public class Connect {
    static String path = "http://ru.wikipedia.org/wiki/%D0%9B%D0%BE%D0%BD%D0%B4%D0%BE%D0%BD";

    public static void main(String[] args) {
        URLConnection connection = null;
        try {
            connection = new URL(path).openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            Pattern pattern = Pattern.compile("(?<=(href=\"))[^\"]*[pdf,mp3]+(?=\")");  //[a-z,A-Z,0-9,/,\-,_,:,\.]
            while (bufferedReader.ready()) {
                Matcher matcher = pattern.matcher(bufferedReader.readLine());
                while (matcher.find()) {
                    final String temp = matcher.group();
                    if(temp.endsWith(".pdf")||temp.endsWith(".mp3")) {
                                System.out.println(temp);
                                connect(temp);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void connect(String path) {
        try {
            URLConnection connection =new URL(path).openConnection();
            download(connection,path);
        } catch (IOException e) {
            System.err.println("Access error: " +path);
        }

    }
    private  static  void download(URLConnection connection,String path) throws IOException {
        BufferedInputStream reader= new BufferedInputStream(connection.getInputStream());
        int i=path.lastIndexOf("/");
        path=path.substring(i+1,path.length());

        File file= new File("src\\files\\"+path);
        if(!file.exists()) {
            FileOutputStream fileWriter = new FileOutputStream(file);
            while ((i = reader.read()) != -1) {
                fileWriter.write(i);
                fileWriter.flush();
            }
            System.out.println("Download file "+path);
            fileWriter.close();
        }
        reader.close();

    }

}


