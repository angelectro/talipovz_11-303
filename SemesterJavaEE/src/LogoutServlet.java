import javax.servlet.ServletException;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Created by ABC on 01.10.14.
 */
public class LogoutServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

                     HttpSession hs=request.getSession();
                     Cookie cookie= new Cookie("user","r");
                     cookie.setMaxAge(0);
                     response.addCookie(cookie);
                     hs.removeAttribute("current_user");


        response.sendRedirect("/home");
    }
}
