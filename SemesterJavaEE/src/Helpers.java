import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by ABC on 01.10.14.
 */
public class Helpers {
    public static  String current_user(HttpServletRequest req)
    {
        HttpSession httpSession=req.getSession();
        return (String) httpSession.getAttribute("current_user");
    }

    public static Cookie hetUserCookie(HttpServletRequest request)
    {
        Cookie[] cookies =request.getCookies();
        for(Cookie cookie:cookies)
        {
            if(cookie.getName().equals("user"))
            {
                return cookie;
            }
        }
        return null;
    }
}
