drop table slider CASCADE;
drop table users CASCADE;
drop table films CASCADE;
drop table timetable CASCADE;
drop table seances CASCADE;
drop table reviews CASCADE;

create table slider
( id serial not null unique,
  img_src char(100) not null,
  film_id int not null
);

create table users
(id serial not null unique,
name char(50),
surname char(50),
email char(50) not null unique,
login char(50) not null unique,
password text not null,
date timestamp default now(),
cookie text not null,
photo text,
access text default 'user'  
);

create table films
(
id serial not null unique,
name text not null ,
year int ,
producer varchar(30) not null,
actors text,
length int not null,
text text,
img text
);

	
create table timetable
(
id serial not null unique,
film_id int not null,
date date not null
);

create table seances
(
id serial not null unique,
timetable_id int not null,
time time not null,
places text 
);


create table reviews
(
id serial not null unique,
header text not null,
film_id int not null,
user_id int not null,
date timestamp default now() not null,
text text);

alter table timetable add constraint timetable_films foreign key (film_id) references films(id);
alter table seances add constraint seances_timetable foreign key (timetable_id) references timetable(id);
alter table reviews add constraint reviews_films foreign key (film_id) references films(id);
alter table reviews add constraint reviews_users foreign key (user_id) references users(id);
ALTER TABLE  slider add CONSTRAINT slider_films FOREIGN KEY (film_id) REFERENCES films(id);




