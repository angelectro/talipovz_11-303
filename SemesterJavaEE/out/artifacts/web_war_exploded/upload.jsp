<%@ page import="org.apache.commons.fileupload.servlet.ServletFileUpload" %>
<%@ page import="org.apache.commons.fileupload.disk.DiskFileItemFactory" %>
<%@ page import="java.io.File" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Iterator" %>
<%@ page import="org.apache.commons.fileupload.FileItem" %>
<%@ page import="java.util.Random" %>
<%@ page import="java.nio.charset.Charset" %>

<%boolean isMultipart = ServletFileUpload.isMultipartContent(request);
    if (!isMultipart) {
        response.sendError(HttpServletResponse.SC_BAD_REQUEST);
        return;
    }

    // Создаём класс фабрику
    DiskFileItemFactory factory = new DiskFileItemFactory();

    // Максимальный буфера данных в байтах,
    // при его привышении данные начнут записываться на диск во временную директорию
    // устанавливаем один мегабайт
    factory.setSizeThreshold(1024 * 1024);

    // устанавливаем временную директорию
    File tempDir = (File) getServletConfig().getServletContext().getAttribute("javax.servlet.context.tempdir");
    factory.setRepository(tempDir);
    ServletFileUpload upload = new ServletFileUpload(factory);
    upload.setSizeMax(1024 * 1024 * 10);

    try {
        List items = upload.parseRequest(request);
        Iterator iter = items.iterator();

        while (iter.hasNext()) {
            FileItem item = (FileItem) iter.next();

            if (item.isFormField()) {
                System.out.println(item.getFieldName() + "=" + item.getString());
            } else {
                File uploadetFile = null;
                Random random = new Random();
                do {
                    pathSource = pathSource +random.nextInt()+ new String(item.getName().getBytes(), Charset.forName("UTF-8"));
                    String path = getServletConfig().getServletContext().getRealPath(pathSource);
                    uploadetFile = new File(path);
                } while (uploadetFile.exists());
                uploadetFile.createNewFile();
                item.write(uploadetFile);
            }
        }

        context.setAttribute("path", pathSource);

    } catch (Exception e) {
        e.printStackTrace();
        response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        return;
    }

%>
