<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 01.11.14
  Time: 4:28
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%

    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    try {
        ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    } catch (NamingException e) {
        e.printStackTrace();
    }
    Connection conn = ds.getConnection();

%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <%-- <script src="http://code.jquery.com/jquery-2.1.0.min.js"></script> --%>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">
    <title>Новинки</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">
        <div style="padding-left: 50px">
            <%
                PreparedStatement statement = conn.prepareStatement("select * from reviews");
                ResultSet set = statement.executeQuery();
                while (set.next()) {
                    String text = set.getString("text");
                    statement = conn.prepareStatement("select name, img from films where id=?");
                    statement.setInt(1, Integer.parseInt(set.getString("film_id")));
                    ResultSet resultSet = statement.executeQuery();
                    resultSet.next();
            %>
            <div class="panel panel-info">
                <div style="width: 10%;margin: 5px; display: inline-block"><img class="img-responsive"
                                                                                src="<%=resultSet.getString("img")%>">
                </div>
                <div style="display: inline-block;width: 65%;vertical-align:top; padding:20px ">
                    <span class="glyphicon glyphicon-user"></span>
                    <a href="film?id=<%=Integer.parseInt(set.getString("film_id"))%>"><%=resultSet.getString("name")%>
                    </a>
                </div>
                <div style="padding: 10px;">
                    <%=set.getString("text")%>
                </div>
                <div class="panel-footer"> Пользователь: Загит Талипов</div>

            </div>
            <%}%>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="rightBar.jsp" %>
    </div>
</div>
</body>
</html>
<%conn.close();%>
