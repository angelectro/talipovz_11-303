<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%--
  Created by IntelliJ IDEA.
  User: ABC
  Date: 29.10.14
  Time: 10:55
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    boolean admin=false;
    boolean user = false;
    int CockID =0;
    Cookie tempCookie = null;
    Cookie cookies[] = request.getCookies();
    for (Cookie cookie : cookies) {
        if (cookie.getName().equals("user")) {
            tempCookie = cookie;
        }

    }

    String ses = (String) session.getAttribute("current_user");
    System.out.println("Куки" + (tempCookie != null ? tempCookie.getValue() : "null"));
    System.out.println("сессия" + (ses == null ? "null" : ses));
    if (tempCookie != null) {

        if ((ses != null && tempCookie.getValue().equals(ses))) {
            user = true;
        } else if ((ses == null)) {
            session.setAttribute("current_user", tempCookie.getValue());
        }
    } else {
        if (ses != null) {
            Cookie cookie = new Cookie("user", ses);
            cookie.setMaxAge(60 * 60 * 24 * 360);

            response.addCookie(cookie);
            user = true;
        }
    }   %>
<nav style="margin-top: 20px" class="navbar navbar-inverse" role="navigation">
    <div class="container-fluid">
        <div class="navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li><a href="home">Главная</a></li>
                <li><a href="schedule">Расписание</a></li>
                <li><a href="new">Новинки</a></li>
                <li><a href="reviews">Рецензии</a></li>
                <li><a href="#">Как проехать</a></li>


            </ul>

            <form class="navbar-form navbar-right" role="sig_in">

                      <%
                    if (!user) {
                %>
                <a href="sigin">
                    <button type="button" class="btn btn-primary">Войти</button>
                </a>
                <%
                } else {
                    PreparedStatement preparedStatement = conn.prepareStatement("select id, name,surname,access from users where cookie=?");
                    try {
                        preparedStatement.setString(1, ses);
                        ResultSet res = preparedStatement.executeQuery();
                        res.next();
                        CockID =res.getInt(1);
                        if(res.getString("access").equals("admin"))
                            admin=true;

                %>


                <div>
                    <div >
                        <span class="glyphicon glyphicon-user"></span>
                        <a href="/personal_cabinet?id=<%=CockID%>"> <%=res.getString(2) + " " + res.getString(3)%>
                        </a>
                    </div>
                    <div style="text-align-last: right">
                        <a  href="/log">Выйти</a>
                    </div>
                </div>
                <% } catch (SQLException e) {
                    e.printStackTrace();
                }
                }%>
            </form>
        </div>
    </div>
</nav>
