<%-- Created by IntelliJ IDEA. --%>
<%@ page import="javax.naming.InitialContext" %>
<%@ page import="javax.naming.NamingException" %>
<%@ page import="javax.sql.DataSource" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.PreparedStatement" %>
<%@ page import="java.sql.ResultSet" %>
<%
    InitialContext initContext = new InitialContext();
    DataSource ds = null;
    try {
        ds = (DataSource) initContext.lookup("java:comp/env/jdbc/postgres");
    } catch (NamingException e) {
        e.printStackTrace();
    }
    Connection conn = ds.getConnection();
%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <script src="\bootstrap\jquery-2.1.1.min.js"></script>
    <script src="\bootstrap\js\bootstrap.js"></script>
    <link href="\bootstrap\css\bootstrap.css" rel="stylesheet" type="text/css">
    <title>Главная</title>
</head>
<body style="background-color: #b0b0b0">
<div class="container">
    <%@include file="navigation.jsp" %>
    <div class="col-xs-12 col-sm-9">

        <div>
            <div id="myCarousel" class="carousel carousel slide" data-ride="carousel">
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                    <li data-target="#myCarousel" data-slide-to="3"></li>
                </ol>
                <div class="carousel-inner">
                    <% PreparedStatement statement = conn.prepareStatement("select film_id,img_src from slider");
                        ResultSet set = statement.executeQuery();
                        int i = 0;
                        while (set.next()) {
                    statement=conn.prepareStatement("select name,text from films where id=?");
                    statement.setInt(1,set.getInt(1));
                    ResultSet resultSet=statement.executeQuery();
                    resultSet.next();%>
                    <div class="item <%=(i == 0 ? "active" : "")%>  ">
                        <img src="<%=set.getString(2)%>" alt="First slide">

                        <div class="container">
                            <div class="carousel-caption">
                                <h1><%=resultSet.getString(1)%></h1>
                            </div>
                        </div>
                    </div>


                    <%
                            i++;
                        }
                        conn.close();
                    %>
                </div>
                <a class="left carousel-control" href="#myCarousel" data-slide="prev"></a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next"></a>
            </div>
        </div>
    </div>
    <div class="col-xs-6 col-sm-3 sidebar-offcanvas" id="sidebar" role="navigation">
        <%@include file="rightBar.jsp" %>
    </div>
    <div></div>
</div>
</body>
</html>

