import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ABC on 01.10.14.
 */
public class AboutMe extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer =new PrintWriter(response.getOutputStream());
        writer.println("<html>\n" +
                "<head>\n" +
                "    <title>AboutMe</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div style='text-align: center'><h1>About ME</h1></div>" +
                Menu.textMenu()+
                "<div style=\"text-align: center; display: inline-block; padding: 10px;vertical-align: top\">\n" +
                "    <table>\n" +
                "        <tr>\n" +
                "            <td style=\"text-align: right\">Имя</td>\n" +
                "            <td style=\"padding-left: 20px;\">Загит</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td style=\"text-align: right\">Никнейм</td>\n" +
                "            <td style=\"padding-left: 20px;\">Zoha</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td style=\"text-align: right\">Email</td>\n" +
                "            <td style=\"padding-left: 20px;\">zahittalipov@gmail.com</td>\n" +
                "        </tr>\n" +
                "        <tr>\n" +
                "            <td style=\"text-align: right; vertical-align: top\">Интересы</td>\n" +
                "            <td style=\"padding-left: 20px;\"><a href=\"http://yandex.ru/yandsearch?text=electroDance\">electroDance</a><br/>\n" +
                "                <a href=\"http://yandex.ru/yandsearch?text=Погода\">Погода</a><br/>\n" +
                "                <a href=\"http://yandex.ru/yandsearch?text=JavaEE\">JavaEE</a></td>\n" +
                "        </tr>\n" +
                "    </table>\n" +
                "</div>" +

                "</body>\n" +
                "</html>");
        writer.close();
    }
}
