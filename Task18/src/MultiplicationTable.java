import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ABC on 01.10.14.
 */
public class MultiplicationTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = new PrintWriter(response.getOutputStream());
        int number=Integer.parseInt(request.getParameter("number"));
        writer.println("<html>\n" +
                "<head>\n" +
                "    <title>Multiplication table</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div style='text-align: center'><h1>Multiplication table</h1></div>" +
                Menu.textMenu()+
                "<div style=\"text-align: center; display: inline-block; padding: 10px;vertical-align: top\">\n" +
                "    <table>\n");
        for(int i=1; i<=number;i++)
        {    writer.println("<tr>");
            for(int j=1; j<=number;j++)
            {
                writer.println("<td>"+(i*j)+"</td>");
            }
            writer.println("</tr>");
        }
        writer.println("</div>" +

                "</body>\n" +
                "</html>");
        writer.close();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = new PrintWriter(response.getOutputStream());
        writer.println("<html>\n" +
                "<head>\n" +
                "    <title>Multiplication table</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div style='text-align: center'><h1>Multiplication table</h1></div>" +
                Menu.textMenu() +
                "<div style=\"text-align: center; display: inline-block; padding: 10px;vertical-align: top\">\n" +
                "<form action='/multiplication_table' method='post'>" +
                "<input type='number' name='number'>" +
                "<input type='submit'></form>" +
                "</div>" +

                "</body>\n" +
                "</html>");
        writer.close();
    }
}
