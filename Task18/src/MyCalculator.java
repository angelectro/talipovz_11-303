import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ABC on 24.09.14.
 */
public class MyCalculator extends HttpServlet {
    private String[] exceptions = {
            "не введен первый аргумент",
            "не введен второй аргумент",
            "первый аргумент не является числом",
            "второй аргумент не является числом",
            "введено отрицательное число для логарифма"
    ,"нельзя делить на ноль"};

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        PrintWriter writer = new PrintWriter(resp.getOutputStream());
        writer.println(
                "\n" +
                "<html>\n" +
                "<head>\n" +
                "    <title>Calculator</title>\n" +
                "</head>\n" +
                "<body>\n" +
                        "<div style='text-align: center'><h1>Calculator</h1></div>" +
                Menu.textMenu()+
                "<div style=\"text-align: center; display: inline-block; padding: 10px;vertical-align: top\">\n" +
                "    <form action=\"/calculator\" method=\"post\">\n" +
                "        <input type=\"text\" name=\"number1\" id=\"number1\">\n" +
                "        <select name=\"operation\" id=\"operation\">\n" +
                "            <option value=\"1-minus\">-</option>\n" +
                "            <option value=\"2-plus\">+</option>\n" +
                "            <option value=\"3-divide\">/</option>\n" +
                "            <option value=\"4-multiply\">*</option>\n" +
                "            <option value=\"5-sin\">sin</option>\n" +
                "            <option value=\"6-cos\">cos</option>\n" +
                "            <option value=\"7-ln\">ln</option>\n" +
                "            <option value=\"8-exp\">exp</option>\n" +
                "        </select>\n" +
                "        <input type=\"text\" name=\"number2\">\n" +
                "        <input type=\"text\" style=\"display: none\" id=\"displayNumber2\" name=\"displayNumber2\">\n" +
                "        <input type=\"submit\">\n" +
                "    </form>\n" +
                "\n" +
                "    <script type=\"text/javascript\" language=\"JavaScript\">\n" +
                "        var op = document.getElementById(\"operation\");\n" +
                "        var number1 = document.getElementById(\"number1\");\n" +
                "        var disp = document.getElementById(\"displayNumber2\");\n" +
                "        var v = parseInt(op.value.substring(0, 1));\n" +
                "        if (v > 4) {\n" +
                "            number1.style.display = \"none\";\n" +
                "            disp.value = \"none\";\n" +
                "        }\n" +
                "        else {\n" +
                "            number1.style.display = \"inline\";\n" +
                "            disp.value = \"inline\";\n" +
                "        }\n" +
                "        op.onchange = function () {\n" +
                "            var v = parseInt(op.value.substring(0, 1));\n" +
                "            if (v > 4) {\n" +
                "                number1.style.display = \"none\";\n" +
                "                disp.value = \"none\";\n" +
                "            }\n" +
                "            else {\n" +
                "                number1.style.display = \"inline\";\n" +
                "                disp.value = \"inline\";\n" +
                "            }\n" +
                "\n" +
                "        }\n" +
                "    </script>\n" +
                "</div>\n" +
                "</body>\n" +
                "</html>");
        writer.close();

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String number1 = req.getParameter("number1");
        String number2 = req.getParameter("number2");
        String dispayNumber2=req.getParameter("displayNumber2");
        int error = -1;
        double result = 0;
        Pattern pattern = Pattern.compile("\\-?((0\\.(0*[1-9]+0*)+)|([1-9][0-9]*\\.(0*[1-9]+0*)+)|([1-9][0-9]*))|0");
        Matcher matcher1 = pattern.matcher(number1);
        Matcher matcher2 = pattern.matcher(number2);
        String operation = req.getParameter("operation");
        int oper = Integer.parseInt(req.getParameter("operation").substring(0, 1));


        if (oper<5&&number1.equals("")) {
            error = 1;
        } else if (number2.equals("")) {
            error = 2;
        } else if (oper<5&&!matcher1.matches()) {
            error = 3;
        } else if (!matcher2.matches()) {
            error = 4;
        } else if (error!=4&&oper == 7 && Double.parseDouble(number2) < 0)
            error = 5;
        else if(error==-1&&oper==3&&Double.parseDouble(number2)==0)
            error=6;


        if (error != -1) {
            error--;
            PrintWriter writer = new PrintWriter(resp.getOutputStream());
            writer.println("<html>\n" +
                    "<head>\n" +
                    "    <title>Calculator</title>\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style='text-align: center'><h1>Calculator</h1></div>" +
                    Menu.textMenu()+
                    "<div style=\"text-align: center; display: inline-block; padding: 10px;vertical-align: top\">\n" +
                    "<p>" + exceptions[error] +
                    "</p> <form action=\"/calculator\" method=\"post\" style=\"text-align: center; \">\n" +
                    "    <input type=\"text\" name=\"number1\" id=\"number1\" value=\"" + number1 +
                    "\" style='display: " + dispayNumber2+
                    "'>\n" +
                    "    <select name=\"operation\" id=\"operation\" >\n" +
                    "        <option " +
                    (oper == 1 ? " selected " : "") +
                    " value=\"1-minus\">-</option>\n" +
                    "        <option" +
                    (oper == 2 ? " selected " : "") +
                    " value=\"2-plus\">+</option>\n" +
                    "        <option " +
                    (oper == 3 ? " selected " : " ") +
                    " value=\"3-divide\">/</option>\n" +
                    "        <option" +
                    (oper == 4 ? " selected " : "") +
                    " value=\"4-multiply\">*</option>\n" +
                    "        <option" +
                    (oper == 5 ? " selected " : "") +
                    " value=\"5-sin\">sin</option>\n" +
                    "        <option" +
                    (oper == 6 ? " selected " : "") +
                    " value=\"6-cos\">cos</option>\n" +
                    "        <option" +
                    (oper == 7 ? " selected " : "") +
                    " value=\"7-ln\">ln</option>\n" +
                    "        <option" +
                    (oper == 8 ? " selected " : "") +
                    " value=\"8-exp\">exp</option>\n" +
                    "    </select>\n" +
                    "    <input type=\"text\" name=\"number2\" value=\"" + number2 +
                    "\">    " +
                    "<input type=\"text\" style=\"display: none\" id=\"displayNumber2\" name=\"displayNumber2\">" +
                    "<input type=\"submit\">\n" +
                    "</form>\n" +
                    "\n" +
                    "<script type=\"text/javascript\" language=\"JavaScript\">\n" +
                    "    var op = document.getElementById(\"operation\");\n" +
                    "    var number1 = document.getElementById(\"number1\");\n" +
                    "    var disp = document.getElementById(\"displayNumber2\");\n" +
                    "    op.onchange = function () {\n" +
                    "        var v = parseInt(op.value.substring(0, 1));\n" +
                    "        if (v > 4) {\n" +
                    "            number1.style.display = \"none\";\n" +
                    "            disp.value= \"none\";\n" +
                    "}\n" +
                    "        else {\n" +
                    "            number1.style.display = \"inline\";\n" +
                    "            disp.value= \"inline\";\n" +
                    "        }\n" +
                    "\n" +
                    "    }\n" +

                    "</script>" +
                    "</div>" +
                    "</body>\n" +
                    "</html>");

            writer.close();
        } else {
            double var1=0;
            if(oper<5)
                 var1 = Double.parseDouble(number1);
            double var2 = Double.parseDouble(number2);
            switch (oper) {
                case 1: {
                    result = var1 - var2;
                    break;
                }
                case 2: {
                    result = var1 + var2;
                    break;
                }
                case 3: {
                    result = var1 / var2;
                    break;
                }
                case 4: {
                    result = var1 * var2;
                    break;
                }
                case 5: {
                    result = Math.sin(var2);
                    break;
                }
                case 6: {
                    result = Math.cos(var2);
                    break;
                }
                case 7: {
                    result = Math.log(var2);
                    break;
                }
                case 8: {
                    result = Math.exp(var2);
                    break;
                }
            }
            resp.sendRedirect("/result?res=" + Double.toString(result));
        }
    }
}
