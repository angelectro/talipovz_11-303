import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ABC on 01.10.14.
 */
public class Menu {

    public static String textMenu()
    {
        SimpleDateFormat format= new SimpleDateFormat("yyyy-MM-dd hh.mm.ss");
        format.format(new Date());
        return "<div style=\"display:inline-block ; padding: 5px; padding-top: 70px;\">\n" +
                "<p style='text-align: center'><h4>Меню</h4> </p>" +
                "    <ul >\n" +
                "        <li><a href=\"/\">Обо мне</a></li>\n" +
                "        <li><a href=\"/calculator\">Мой online-калькулятор</a></li>\n" +
                "        <li><a href=\"/multiplication_table\">Моя таблица умножения</a></li>\n" +
                "    </ul>\n" +
                "\n" + format.format(new Date()) +
                "</div>";
    }
}
