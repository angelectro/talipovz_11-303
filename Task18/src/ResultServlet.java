import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ABC on 30.09.14.
 */
public class ResultServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = new PrintWriter(response.getOutputStream());
        String s = request.getParameter("res");
        writer.println("<html>\n" +
                "<head>\n" +
                "    <title>Calculator</title>\n" +
                "</head>\n" +
                "<body>\n" +
                "<div style='text-align: center'>Calculator</div>" +

                Menu.textMenu() +
                "<div style=\"text-align: center; display: inline-block; padding: 10px;vertical-align: top\">\n" +
                "<p style='text-align: center'><t1> Result:  " + s+
                "</t1></p>" +

                "</div>" +

                "</body>\n" +
                "</html>");
        writer.close();
    }
}
