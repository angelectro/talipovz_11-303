/**
 * Created by ABC on 20.10.14.
 */
public class Teacher {
    private String id;
    private String name;
    private School school;

    public Teacher(String id, String name,School school)
    {
        setName(name);
        setId(id);
        setSchool(school);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public School getSchool() {
        return school;
    }

    public void setSchool(School school) {
        this.school = school;
    }

    public String getId() {

        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
