/**
 * Created by ABC on 20.10.14.
 */
public class Student {
      private String name;
      private String id;

    public Student(String id,String name)
    {
        setName(name);
        setId(id);
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
