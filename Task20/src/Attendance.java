/**
 * Created by ABC on 20.10.14.
 */
public class Attendance {
    private School_Class school_class;
    private Teacher teacher;
    private int year;
    private Student student;

    public Attendance(School_Class school_class,Teacher teacher, int year,Student student)
    {
        setSchool_class(school_class);
        setStudent(student);
        setTeacher(teacher);
        setYear(year);
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Teacher getTeacher() {
        return teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }

    public School_Class getSchool_class() {
        return school_class;
    }

    public void setSchool_class(School_Class school_class) {
        this.school_class = school_class;
    }

    @Override
    public String toString() {
        return school_class.getName()+ " "+teacher.getName()+ " "+year+" "+ student.getName();
    }
}
