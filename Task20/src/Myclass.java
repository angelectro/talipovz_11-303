import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by ABC on 13.10.14.
 */
public class Myclass {
    public static void main(String args[]) throws SQLException {
        Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://127.0.0.1:5432/work", "postgres", "amina"
        );
        HashMap<String, Student> students = new HashMap<String, Student>();
        HashMap<String, School_Class> school_classes = new HashMap<String, School_Class>();
        HashMap<String, School> schools = new HashMap<String, School>();
        HashMap<String, Teacher> teachers = new HashMap<String, Teacher>();
        ArrayList<Attendance> attendance = new ArrayList<Attendance>();

        PreparedStatement statement = connection.prepareStatement("select * from students");

        ResultSet resultSet = statement.executeQuery();
        while (resultSet.next()) {
            students.put(resultSet.getString("id"), new Student(resultSet.getString("id"), resultSet.getString("name")));
        }

        statement = connection.prepareStatement("select * from classes");
        resultSet = statement.executeQuery();
        while (resultSet.next()) {
            school_classes.put(resultSet.getString("name"), new School_Class(resultSet.getString("name")));
        }

        statement = connection.prepareStatement("select * from schools");
        resultSet = statement.executeQuery();
        while (resultSet.next()) {
            schools.put(resultSet.getString("name"), new School(resultSet.getString("name")));
        }

        statement = connection.prepareStatement("select teachers.id,teachers.name,schools.name from teachers,schools where school_id=schools.id");
        resultSet = statement.executeQuery();
        while (resultSet.next()) {
            teachers.put(resultSet.getString(1), new Teacher(resultSet.getString(1), resultSet.getString(2), schools.get(resultSet.getString(3))));
        }

        statement = connection.prepareStatement("select classes.name,teachers.id,year,students.id from classes,teachers,attendance,students" +
                " where class_id=classes.id and teacher_id=teachers.id and student_id=students.id");
        resultSet = statement.executeQuery();
        while (resultSet.next()) {
            attendance.add(new Attendance(school_classes.get(resultSet.getString(1)), teachers.get(resultSet.getString(2)), Integer.parseInt(resultSet.getString(3)), students.get(resultSet.getString(4))));
        }

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter("1980.txt"));
            Iterator<Attendance> iterator = attendance.iterator();

            while (iterator.hasNext()) {
                Attendance temp = iterator.next();
                if (temp.getYear() >= 1980) {
                    writer.write(temp.toString() + "\n");
                }
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }


        resultSet.close();
    }
}
