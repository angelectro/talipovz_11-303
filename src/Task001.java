import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Zagit Talipov
 * 11-303
 * 001
 */

public class Task001 {

    public static void main(String args[])
    {
        String[] numbers={"834(123)343-23-23",
                "8(123)34323-23",
                "8123)343-23-23",
                "8(123)3-23-23"};

        Pattern pattern= Pattern.compile("[0-9]{1,3} ?\\(\\d{3}\\) ?\\d{3}(-\\d{2}){2}");
        Matcher matcher;
        for(String s: numbers)
        {
        matcher=pattern.matcher(s);
        System.out.println(matcher.matches());
        }
    }
}
