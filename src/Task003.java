import com.sun.swing.internal.plaf.metal.resources.metal;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Zagit Talipov
 * 11-303
 * 003
 */
public class Task003 {
   public static void main(String args[])
   {
       int total_generated=0;
       int result_true=0;
       Random random= new Random();
       Pattern pattern;
       Matcher matcher;
       while (result_true!=10)
       {   total_generated++;
           String current=Integer.toString(Math.abs(random.nextInt()));
           pattern=Pattern.compile("[0-9]*[02468]");
           matcher=pattern.matcher(current);
           if(matcher.matches())
           {
               result_true++;
               System.out.println(matcher.group());
           }
       }
       System.out.println("Total generated:" +total_generated);
   }

}
