import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Zagit Talipov
 *         11-303
 *         011
 */
public class Task011 {
    private static LinkedList<String> found;

    public static void main(String args[]) {
        found = new LinkedList<String>();
        Pattern pattern = Pattern.compile("([a-zA-Z0-9\\-]+(?=\\=\".*\"))");
        try {
            BufferedReader reader = new BufferedReader(new FileReader("src\\view-source.htm"));
            while (reader.ready()) {
                String line = reader.readLine();
                Matcher matcher = pattern.matcher(line);
                while (matcher.find()) {
                    String temp = matcher.group();
                    if (!found.contains(temp)) {
                        found.add(temp);
                        System.out.println(temp);
                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

}
