import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by ABC on 15.09.14.
 */
public class Task016 {
    static String reg[] = {"/admin", "(?<=/profile)[0-9]+", "(?<=\\/entry)(((199[1-9])|(200[0-9])|(201[0-4]))-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01]))"};
    static String address[] = {"admin.html", "profile.html", "entry.html", "404.html"};
    static String text[] = {"Administrator", " Это профиль человека #", "Запись дневника от ", " Ошибка 404"};

    public static void main(String args[]) {
        Scanner sc = new Scanner(System.in);
        try {
            BufferedWriter writer;
            int i = 0;
            String result = null;
            String s = sc.next();
            for (String current : reg) {
                Pattern pattern = Pattern.compile(current);
                Matcher matcher = pattern.matcher(s);
                if (!matcher.find()) {
                    i++;
                } else {
                    System.out.println(i);
                    result = matcher.group();
                    break;
                }
            }
            if (i == 2) {
                String temp = "";
                Pattern pattern = Pattern.compile("[0-9]+(?=-|$)");
                Matcher matcher = pattern.matcher(result);
                while (matcher.find()) {
                    temp = matcher.group() + "." + temp;
                }
                result = temp.substring(0, temp.length() - 1);
            }
            writer = new BufferedWriter(new FileWriter(address[i]));
            writer.write("<!DOCTYPE html>\n" +
                    "<html>\n" +
                    "<head>\n" +
                    "    <title>\n" +
                    "    </title>\n" +
                    "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=UTF-8\">" +
                    "</head>\n" +
                    "<body>\n" +
                    "<div style=\"text-align: center; font-size: 50px; margin-top: 20%\">\n" +
                    text[i] + ((i == 1 | i == 2) ? result : "") +
                    "</div>\n" +
                    "</body>\n" +
                    "\n" +
                    "</html>");
            writer.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
