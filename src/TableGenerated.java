import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Zagit Talipov
 * 11-303
 * for task012
 */
public class TableGenerated {

    public static void main(String args[]) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("src\\Task012.html"));
        writer.write("<table border='1'>");
        for (int i = 1; i <= 10; i++) {
            if (i == 6 || i == 1) {
                writer.write("<tr>");
            }
            writer.write("<td>");
            for (int j = 1; j <=10; j++) {
                writer.write(Integer.toString(i) + " x " + Integer.toString(j) + " = " + Integer.toString(i * j) + "</br>");
            }
            writer.write("</td>");
            if (i == 10 || i == 5) {
                writer.write("</tr>");
            }
        }
        writer.write("</table>");
        writer.close();
    }
}
