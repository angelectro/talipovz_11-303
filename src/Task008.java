import com.sun.swing.internal.plaf.metal.resources.metal;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Zagit Talipov
 * 11-303
 * 008
 */
public class Task008 {
    public static void main(String args[])
    {
        int total_generated=0;
        int result_true=0;
        Random random= new Random();
        Pattern pattern;
        Matcher matcher;
        while (result_true!=10)
        {   total_generated++;
            String current=Integer.toString(Math.abs(random.nextInt(0)));
            pattern=Pattern.compile("(((.*[13579]+[02468]{2}|[02468]{2}))([13579]+|$)){2,}");
            matcher=pattern.matcher(current);
            if(matcher.find())
            {
                result_true++;
                System.out.println(matcher.group());
            }
        }
        System.out.println("Total generated:" +total_generated);
    }

}
