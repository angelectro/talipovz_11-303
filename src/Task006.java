import com.sun.swing.internal.plaf.metal.resources.metal;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Zagit Talipov
 * 11-303
 * 006
 */
public class Task006 {
    public static void main(String args[])
    {
        int total_generated=0;
        int result_true=0;
        Random random= new Random();
        Pattern pattern;
        Matcher matcher;
        while (result_true!=10)
        {   total_generated++;
            String current=Integer.toString(Math.abs(random.nextInt()));
            pattern=Pattern.compile("([02468]([13579]|&))*|([13579]([02468]|&))*");
            matcher=pattern.matcher(current);
            if(matcher.matches())
            {
                result_true++;
                System.out.println(current);
            }
        }
        System.out.println("Total generated:" +total_generated);
    }

}
