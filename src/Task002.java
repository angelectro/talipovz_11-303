import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Zagit Talipov
 * 11-303
 * 002
 */
public class Task002 {
    public static void main(String args[])
    {
        String[] numbers={"+7-123-456-78-90",
                "+7-123-45-78-90",
                "+7-123-45678-90",
                "8(123)3-23-23"};
        Pattern pattern= Pattern.compile("\\+7-\\d{3}-\\d{3}(-\\d{2}){2}");
        Matcher matcher;
        for(String s: numbers)
        {
            matcher=pattern.matcher(s);
            System.out.println(matcher.matches());
        }
    }
}