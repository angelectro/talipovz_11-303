<%-- Created by IntelliJ IDEA. --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>Calculator</title>
</head>
<body>
<div style="text-align: center">
<form action="/" method="post">
    <input type="text" name="number1" id="number1">
    <select name="operation" id="operation">
        <option value="1-minus">-</option>
        <option value="2-plus">+</option>
        <option value="3-divide">/</option>
        <option value="4-multiply">*</option>
        <option value="5-sin">sin</option>
        <option value="6-cos">cos</option>
        <option value="7-ln">ln</option>
        <option value="8-exp">exp</option>
    </select>
    <input type="text" name="number2">
    <input type="text" style="display: none" id="displayNumber2" name="displayNumber2">
    <input type="submit">
</form>

<script type="text/javascript" language="JavaScript">
    var op = document.getElementById("operation");
    var number1 = document.getElementById("number1");
    var disp = document.getElementById("displayNumber2");
    var v = parseInt(op.value.substring(0, 1));
    if (v > 4) {
        number1.style.display = "none";
        disp.value= "none";
    }
    else {
        number1.style.display = "inline";
        disp.value= "inline";
    }
    op.onchange = function () {
        var v = parseInt(op.value.substring(0, 1));
        if (v > 4) {
            number1.style.display = "none";
            disp.value= "none";
}
        else {
            number1.style.display = "inline";
            disp.value= "inline";
        }

    }
</script>
</div>
</body>
</html>